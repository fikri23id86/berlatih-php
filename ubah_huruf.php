<?php
function ubah_huruf($string){
//kode di sini
	$temp = "";
	for($i = 0 ; $i < strlen($string); $i++){
		$stringASCII = ord($string[$i]);
		$stringChar = chr($stringASCII+1);
		$temp = $temp.$stringChar;
	}
	return $temp;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>