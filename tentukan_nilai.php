<?php
function tentukan_nilai($number)
{
    //  kode disini
    if($number >= 85 && $number<=100)
    	return "Sangt Baik";
    else if ($number >= 70)
    	return " Baik";
    else if ($number >= 60)
    	return " Cukup";
    else
    	return " Kurang";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>